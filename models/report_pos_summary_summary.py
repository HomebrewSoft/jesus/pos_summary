# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class ReportSummary(models.AbstractModel):
    _name = 'report.pos_summary.summary'
    _description = 'Report PoS Summary'

    def _get_orders(self, start_date, end_date, configs):
        order_obj = self.env['pos.order']
        orders = order_obj.search([
            ('config_id', 'in', configs.ids),
            ('date_order', '>=', start_date),
            ('date_order', '<=', end_date),
        ])
        return orders

    def _get_orders_value(self, orders):
        return sum(orders.mapped('amount_paid'))

    def _get_start_end_names(self, orders):
        names = sorted(orders.mapped('name'))
        if names:
            return names[0], names[-1]
        return '', ''

    def _list_to_records(self, l, model='pos.order.line'):
        return self.env[model].browse([r.id for r in l])

    def _get_order_lines(self, orders):
        lines = [line for lines in orders.mapped('lines') for line in lines]
        return self._list_to_records(lines)

    def _get_lines_by_tax(self, lines):
        res = {}
        for line in lines:
            for tax in line.tax_ids_after_fiscal_position or ['']:
                if not res.get(tax):
                    res[tax] = []
                res[tax].append(line)
        return {k: self._list_to_records(v) for k, v in res.items()}

    def _get_orders_by_payment(self, orders):
        res = {}
        for order in orders:
            for payment in order.payment_ids:
                if payment.amount > 0:
                    if not res.get(payment.payment_method_id):
                        res[payment.payment_method_id] = []
                    res[payment.payment_method_id].append(order)
        return {k: self._list_to_records(v, 'pos.order') for k, v in res.items()}

    def _get_lines_by_categ_tax(self, lines):
        res = {}
        for line in lines:
            for tax in line.tax_ids_after_fiscal_position or ['']:
                if not res.get((line.product_id.categ_id, tax)):
                    res[(line.product_id.categ_id, tax)] = []
                res[(line.product_id.categ_id, tax)].append(line)
        return {k: self._list_to_records(v) for k, v in res.items()}

    def _get_categ_tax_group(self, groups):
        res = [{
            'category_id': group[0],
            'tax_id': group[1],
            'discount': sum(lines.mapped(lambda l: l.price_subtotal / (1 - l.discount / 100) - l.price_subtotal)),
            'price_subtotal': sum(lines.mapped('price_subtotal')),
            'price_subtotal_incl': sum(lines.mapped('price_subtotal_incl')),
        } for group, lines in groups.items()]
        return res

    def _get_tax_group(self, groups):
        res = [{
            'tax_id': group,
            'discount': sum(lines.mapped(lambda l: l.price_subtotal / (1 - l.discount / 100) - l.price_subtotal)),
            'price_subtotal': sum(lines.mapped('price_subtotal')),
            'price_subtotal_incl': sum(lines.mapped('price_subtotal_incl')),
        } for group, lines in groups.items()]
        return res

    def _get_payment_group(self, groups):
        res = [{
            'payment_id': group,
            'amount': sum(orders.mapped('amount_total')),
        } for group, orders in groups.items()]
        return res

    def _get_data(self, start_date, end_date, configs):
        orders = self._get_orders(start_date, end_date, configs)
        orders_value = self._get_orders_value(orders)
        start_name, end_name = self._get_start_end_names(orders)
        lines = self._get_order_lines(orders)
        lines_by_tax = self._get_lines_by_tax(lines)
        lines_by_categ_tax = self._get_lines_by_categ_tax(lines)
        categ_tax_group = self._get_categ_tax_group(lines_by_categ_tax)
        tax_group = self._get_tax_group(lines_by_tax)
        orders_by_payment = self._get_orders_by_payment(orders)
        payment_group = self._get_payment_group(orders_by_payment)
        data = {
            'current_date': fields.Datetime.context_timestamp(self, fields.Datetime.now()),
            'order_ids': orders,
            'orders_len': len(orders),
            'orders_value': orders_value,
            'start_name': start_name,
            'end_name': end_name,
            'lines_by_tax': lines_by_tax,
            'orders_by_payment': orders_by_payment,
            'categ_tax_group': categ_tax_group,
            'tax_group': tax_group,
            'payment_group': payment_group,
            'exempt_string': _('Exempt'),
        }
        return data

    @api.model
    def _get_report_values(self, docids, data=None):
        data = dict(data or {})
        print(data['config_ids'])
        configs = self.env['pos.config'].browse([int(id) for id in list(data['config_ids'])])
        extra_data = self._get_data(data['start_date'], data['end_date'], configs)
        data.update(extra_data)
        return data
