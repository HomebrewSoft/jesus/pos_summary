# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class SummaryWizard(models.TransientModel):
    _name = 'pos.summary_wizard'
    _description = 'PoS Summary Wizard'

    start_date = fields.Datetime(
        required=True,
        default=fields.Datetime.now,
    )
    end_date = fields.Datetime(
        required=True,
        default=fields.Datetime.now,
    )
    pos_config_ids = fields.Many2many(
        comodel_name='pos.config',
        default=lambda self: self.env['pos.config'].search([]),
    )

    def generate_report(self):
        data = {'start_date': self.start_date, 'end_date': self.end_date, 'config_ids': self.pos_config_ids.ids}
        return self.env.ref('pos_summary.summary_report').report_action([], data=data)
