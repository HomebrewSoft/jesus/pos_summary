# -*- coding: utf-8 -*-
{
    'name': 'PoS Summary',
    'version': '13.0.0.1.0',
    'author': 'HomebrewSoft',
    'website': 'https://homebrewsoft.dev/',
    'license': 'LGPL-3',
    'depends': [
        'point_of_sale',
    ],
    'data': [
        # security
        # data
        # reports
        'reports/report_pos_summary.xml',
        # views
        'views/pos_summary_wizard.xml',
    ],
}
